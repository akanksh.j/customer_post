package main

import (
	"fmt"
	"net/http"
	"strconv"
)

func CustomerDetail(w http.ResponseWriter, r* http.Request) {
	if r.Method==http.MethodPost {
		name:=r.FormValue("name")
		age:=r.FormValue("age")
		age1,_ :=strconv.Atoi(age)
		fmt.Fprintf(w,"%s = %v",name,age1+5)
	}
}
func main() {
	http.HandleFunc("/",CustomerDetail)
	http.ListenAndServe(":8080",nil)
}
